# TicTacTrip Technical Test

Technical test for [TicTacTrip](https://www.tictactrip.eu/)

## Requirments

All requirments are available [here](https://docs.google.com/document/d/1DW2fKidrl5qV1ZvpmysqEMpoqbYjaRQhOJneYnfhzNs/edit)

## Live demo

A live demo is available [here](https://tictactrip-test.herokuapp.com/) provided by [heroku](https://heroku.com)

## Installation

```bash
yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## CI/CD

We use gitlab ci to perform continous integration and continous delivery.

Each pull request will have to pass unit test, end to end test and be conformed to `prettier` format.

Each merge to `master` launch a deployment to heroku production.
