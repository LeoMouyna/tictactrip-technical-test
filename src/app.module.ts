import { Module } from "@nestjs/common";
import { AuthModule } from "./auth/auth.module";
import { JustifyModule } from "./justify/justify.module";

@Module({
	imports: [AuthModule, JustifyModule],
})
export class AppModule {}
