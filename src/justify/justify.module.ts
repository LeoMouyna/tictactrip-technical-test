import { Module } from "@nestjs/common";
import { JustifyService } from "./justify.service";
import { JustifyController } from "./justify.controller";
import { AuthModule } from "src/auth/auth.module";

@Module({
	imports: [AuthModule],
	providers: [JustifyService],
	controllers: [JustifyController]
})
export class JustifyModule {}
