import { Injectable } from "@nestjs/common";

@Injectable()
export class JustifyService {

	LINE_LENGTH = +process.env.JUSTIFY_LINE_LENGTH || 80;

	#cleanParagraph(paragraph: string): string {
		const multiSpacesRegex = / +/gm;
		return paragraph
			.replaceAll("\n", " ")
			.replaceAll(multiSpacesRegex, " ")
			.trim();
	}

	#breakLinesWithWords(text: string): string[][] {
		const words = text.split(" ");
		return words.reduce((prev, curr) => {
			const lineList = [...prev];
			const lastLineIndex = lineList.length - 1;
			const lastLine = lineList[lastLineIndex];

			if(lastLine.length === 0) {
				lineList[lastLineIndex] = [curr];
				return lineList;
			}

			const extendedLine = [lastLine, curr].join(" ").trim();
			if (extendedLine.length > this.LINE_LENGTH) {
				return [...lineList, [curr]];
			}

			lineList[lastLineIndex].push(curr);
			return lineList;
		}, [[]]);
	}

	#breakParagraphes(text: string): string[] {
		return text.split("\n\n");
	}

	#populateLine(words: string[]): string {
		const line = words.join(" ");
		if (line.length === this.LINE_LENGTH) return line;
		const nbDefaultSpaces = words.length - 1;
		const nbChars = line.length - (nbDefaultSpaces);
		const nbMissgingSpaces = this.LINE_LENGTH - nbChars; 
		const nbRemindedSpaces = nbMissgingSpaces % nbDefaultSpaces;
		const spaceRatio = (nbMissgingSpaces - nbRemindedSpaces) / nbDefaultSpaces;

		return words.reduce((acc, curr, index) => {
			const nbWhiteSpaces = index <= nbRemindedSpaces ? spaceRatio + 1 : spaceRatio; 
			const whiteSpaces = new Array(nbWhiteSpaces).fill(" ").join("");
			return [acc, curr].join(whiteSpaces);
		});

	}

	#justifyLine(line: string[], {lineIndex, nbLines}: {lineIndex: number, nbLines: number}): string {
		if (lineIndex + 1 === nbLines) return line.join(" ");
		return this.#populateLine(line);
	}

	#justifyParagraph(paragraph: string): string {
		return this.#breakLinesWithWords(paragraph).map((line, index, lines) => this.#justifyLine(line, {lineIndex:index, nbLines: lines.length})).join("\n");
	}

	#generateParagraphes(text: string): string[] {
		return this.#breakParagraphes(text).map(p => this.#cleanParagraph(p));
	}

	justify(text: string): string {
		return this.#generateParagraphes(text).map(paragraph => this.#justifyParagraph(paragraph)).join("\n");
	}
}
