import { Test, TestingModule } from "@nestjs/testing";
import { JustifyService } from "./justify.service";

describe("JustifyService", () => {
	let service: JustifyService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [JustifyService],
		}).compile();

		service = module.get<JustifyService>(JustifyService);
	});

	it("should be defined", () => {
		expect(service).toBeDefined();
	});

	describe("justify", () => {
		beforeEach(() => {
			service.LINE_LENGTH = 20;
		});
		describe("with already well formated input", () => {
			it("return same text", () => {
				const input = "Un jour sur la plage";
				const formated = service.justify(input);
				expect(formated).toBe(input);
			});
		});
		describe("with a too long input for one line", () => {
			it("return formated text", () => {
				const input = "Un jour sur la plage.";
				const expected = "Un   jour   sur   la\nplage.";
				const formated = service.justify(input);
				expect(formated).toBe(expected);
			});
		});
		describe("with euclidian reminder", () => {
			it("allocate spaces at the begining first", () => {
				const input = "Un jour, nous allions sur la plage.";
				const expected = "Un     jour,    nous\nallions    sur    la\nplage.";
				const formated = service.justify(input);
				expect(formated).toBe(expected);
			});
		});
		describe("with short paragraphes", () => {
			it("return formated text with cariage return", () => {
				const input = "Sous le vent.\n\nLa mer est belle.";
				const expected = "Sous le vent.\nLa mer est belle.";
				const formated = service.justify(input);
				expect(formated).toBe(expected);
			});
		});
		describe("with long paragraphes", () => {
			it("return formated text with cariage return", () => {
				const input = "Un jour, nous allions sur la plage.\n\nLa mer etait belle et le soleil resplendissant.";
				const expected = "Un     jour,    nous\nallions    sur    la\nplage.\nLa  mer  etait belle\net     le     soleil\nresplendissant.";
				const formated = service.justify(input);
				expect(formated).toBe(expected);
			});
		});
		describe("with line breaks inside paragraph", () => {
			it("clean paragraph to format it", () => {
				const input = "Un\njour,\n nous\nallions sur la plage.";
				const expected = "Un     jour,    nous\nallions    sur    la\nplage.";
				const formated = service.justify(input);
				expect(formated).toBe(expected);
			});
		});
	});
});
