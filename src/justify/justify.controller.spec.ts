import { Test, TestingModule } from "@nestjs/testing";
import { JustifyController } from "./justify.controller";
import { JustifyService } from "./justify.service";

describe("JustifyController", () => {
	let controller: JustifyController;
	let justifyService: JustifyService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			controllers: [JustifyController],
			providers: [JustifyService]
		}).compile();

		controller = module.get<JustifyController>(JustifyController);
		justifyService = module.get<JustifyService>(JustifyService);
	});

	it("should be defined", () => {
		expect(controller).toBeDefined();
	});

	describe("format", () => {
		it("call justify from justifyService", async () => {
			jest.spyOn(justifyService, "justify").mockReturnValueOnce("test");
			await controller.format("test");
			expect(justifyService.justify).toBeCalledTimes(1);
			expect(justifyService.justify).toHaveBeenCalledWith("test");
		});
		it("return justifyService justify return", async() => {
			const output = "Well formated text";
			jest.spyOn(justifyService, "justify").mockReturnValueOnce(output);
			const res = await controller.format("test");
			expect(res).toBe(output);
		});
	});
});
