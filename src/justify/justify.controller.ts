import { Controller, Post, UseGuards } from "@nestjs/common";
import { ApiBadRequestResponse, ApiBearerAuth, ApiBody, ApiCreatedResponse, ApiOperation, ApiTags } from "@nestjs/swagger";
import { TextPlainBody } from "src/common/decorators/text-plain-body.decorator";
import { JwtGuard } from "src/common/guards/jwt.guard";
import { JustifyService } from "./justify.service";

@Controller("api/justify")
export class JustifyController {
	constructor(private readonly justifyService: JustifyService) {}

    @UseGuards(JwtGuard)
	@ApiBearerAuth()
    @Post()
    @ApiOperation({ description: "Format input text." })
    @ApiCreatedResponse({description: "Return justified input text."})
	@ApiBadRequestResponse({description: "Wrong request format"})
	@ApiTags("private")
    @ApiBody({schema: {type: "string"}})
	async format(@TextPlainBody() text: string): Promise<string> {
		return this.justifyService.justify(text);
	}
}
