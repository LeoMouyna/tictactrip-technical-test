export const jwtConstants = {
	secret: process.env.JWT_SECRET || "MySuperSecret!",
	expiration: process.env.JWT_EXPIRATION || "60s"
};
  