import { BadRequestException, createParamDecorator, ExecutionContext } from "@nestjs/common";
import * as getRawBody from "raw-body";

export const TextPlainBody = createParamDecorator( async (_, ctx: ExecutionContext) => {
	const request = ctx.switchToHttp().getRequest();

	if (!request.readable) { throw new BadRequestException("Invalid body"); }

	const body = (await getRawBody(request)).toString("utf8").trim();
	return body;
});
