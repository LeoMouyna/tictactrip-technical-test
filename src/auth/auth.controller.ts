import { Body, Controller, Post } from "@nestjs/common";
import { ApiBadRequestResponse, ApiCreatedResponse, ApiOperation, ApiTags } from "@nestjs/swagger";
import { AuthService } from "./auth.service";
import { CreateTokenDto } from "./dto/create-token.dto";

@Controller("api/token")
export class AuthController {
	constructor( private readonly authService: AuthService) {}

    @Post()
    @ApiOperation({ description: "Generate temporary JSON Web Token." })
    @ApiCreatedResponse({description: "Return a temporary JSON Web Token"})
	@ApiBadRequestResponse({description: "Wrong request format"})
	@ApiTags("public")
	async getToken(@Body() {email}: CreateTokenDto): Promise<{token: string}> {
		return this.authService.generateToken(email);
	}
}
