import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { Test, TestingModule } from "@nestjs/testing";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";

describe("AuthController", () => {
	let controller: AuthController;
	let authService: AuthService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			imports:[PassportModule, JwtModule.register({
				secret: "jwtConstants.secret",
				signOptions: { expiresIn: "60s"},
			})],
			controllers: [AuthController],
			providers:[AuthService]
		}).compile();

		controller = module.get<AuthController>(AuthController);
		authService = module.get<AuthService>(AuthService);
	});

	it("should be defined", () => {
		expect(controller).toBeDefined();
	});

	describe("getToken", () => {
		it("should call AuthService generateToken", async () => {
			const token = "testToken5678";
			jest.spyOn(authService, "generateToken").mockResolvedValueOnce({token});
			await controller.getToken({email: "leo@mouyna.fr"});
			expect(authService.generateToken).toHaveBeenCalledTimes(1);
		});
		it("should return generated token", async () => {
			const token = "testToken5678";
			jest.spyOn(authService, "generateToken").mockResolvedValueOnce({token});
			const res = await controller.getToken({email: "leo@mouyna.fr"});
			expect(res).toMatchObject({token});
		});
	});
});
