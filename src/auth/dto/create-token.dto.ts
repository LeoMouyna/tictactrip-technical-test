import { ApiProperty } from "@nestjs/swagger";
import { IsEmail } from "class-validator";

export class CreateTokenDto {
  @ApiProperty({format: "email"})
  @IsEmail()
  email: string;
}
