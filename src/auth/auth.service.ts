import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";

@Injectable()
export class AuthService {
	constructor(private jwtService: JwtService) {}

	async generateToken(email: string): Promise<{token: string}> {
		const token = this.jwtService.sign({email});
		return {token};
	}
}
