import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { Test, TestingModule } from "@nestjs/testing";
import { AuthService } from "./auth.service";
import { JwtStrategy } from "../common/strategies/jwt.strategy";

const secret = "MySuperSecret!";
const expiration = "60s";

describe("AuthService", () => {
	let service: AuthService;
	const jwtConstants ={
		secret,
		expiration
	};

	beforeAll(async () => {
		const module: TestingModule = await Test.createTestingModule({
			imports: [
				PassportModule,
				JwtModule.register({
					secret: jwtConstants.secret,
					signOptions: { expiresIn: jwtConstants.expiration },
				}),
			],
			providers: [AuthService, JwtStrategy],
		}).compile();

		service = module.get<AuthService>(AuthService);
	});

	it("should be defined", () => {
		expect(service).toBeDefined();
	});

	describe("generateToken", () => {
		it("should return JWT object", async () => {
			const res = await service.generateToken("leo@mouyna.fr");
			expect(res.token).toBeDefined();
		});
	});
});
