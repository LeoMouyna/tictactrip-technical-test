import { ValidationPipe } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { AppModule } from "./app.module";

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.useGlobalPipes(new ValidationPipe());

	const APP_VERSION = process.env.npm_package_version;

	const config = new DocumentBuilder()
		.setTitle("TicTacTrip Technical Test")
		.setDescription("TicTacTrip technical test that provide secure API to justify text")
		.setVersion(APP_VERSION)
		.addBearerAuth()
		.build();
	const document = SwaggerModule.createDocument(app, config);
	SwaggerModule.setup("", app, document);

	await app.listen(process.env.PORT || 3000);
}
bootstrap();
