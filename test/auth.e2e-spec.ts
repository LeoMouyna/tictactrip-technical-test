import * as request from "supertest";
import { Test } from "@nestjs/testing";
import { INestApplication, ValidationPipe } from "@nestjs/common";
import { AuthModule } from "./../src/auth/auth.module";
import { AuthService } from "./../src/auth/auth.service";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";

describe("AuthController (e2e)", () => {
	let app: INestApplication;

	beforeAll(async () => {
		const moduleFixture = await Test.createTestingModule({
			imports: [AuthModule, PassportModule, JwtModule.register({
				secret: "jwtConstants.secret",
				signOptions: { expiresIn: "60s"},
			})],
			providers: [AuthService]
		}).compile();

		app = moduleFixture.createNestApplication();
		app.useGlobalPipes(new ValidationPipe());
		await app.init();
	});

	describe("POST /api/token", () => {
		describe("when email address is not provided", () => {
			it("responds with status code 400", () => {
				return request(app.getHttpServer())
					.post("/api/token")
					.expect(400);
			});
		});
		describe("when email address doesn't match email format", () => {
			it("responds with status code 400", () => {
				return request(app.getHttpServer())
					.post("/api/token")
					.send({email: "test"})
					.expect(400);
			});
		});
		describe("when email address is provided", () => {
			it("responds with new token", async () => {
				const res = await request(app.getHttpServer())
					.post("/api/token")
					.send({email: "leo@mouyna.fr"});
				expect(res.status).toBe(201);
				expect(res.body).toMatchObject({token: expect.any(String)});
			});
		});
	});
});


