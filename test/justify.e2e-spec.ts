import * as request from "supertest";
import { Test } from "@nestjs/testing";
import { INestApplication } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { JustifyModule } from "./../src/justify/justify.module";
import { JustifyService } from "./../src/justify/justify.service";
import { readFile } from "fs/promises";

describe("JustifyController (e2e)", () => {
	let app: INestApplication;
	const jwtService = new JwtService({secret: "MySuperSecret!",
		signOptions: { expiresIn: "60s"}});

	beforeAll(async () => {
		const moduleFixture = await Test.createTestingModule({
			imports: [JustifyModule],
			providers: [JustifyService]
		}).compile();

		app = moduleFixture.createNestApplication();
		await app.init();
	});

	describe("POST /api/justify", () => {
		describe("As anonymous user", () => {
			it("responds with status code 401", () => {
				return request(app.getHttpServer())
					.post("/api/justify")
					.set("Content-Type", "text/plain")
					.send("Justify test")
					.expect(401);
			});
		});
		describe("As authenticated user", () => {
			const jwt = jwtService.sign({email: "leo@mouyna.fr"});
			it("responds with formated text", async () => {
				const input = await readFile("test/resources/justify.input.txt", "utf-8");
				const output = await readFile("test/resources/justify.output.txt", "utf-8");
				return request(app.getHttpServer())
					.post("/api/justify")
					.set("Content-Type", "text/plain")
					.set("Authorization", `Bearer ${jwt}`) 
					.send(input)
					.expect(201)
					.expect(output);
			});
		});
	});
});